#!/usr/bin/env bash
#
# Run this file to install the dotfiles.

# Create $HOME/bin, if it doesn't already exist
if [ ! -d "$HOME/bin" ]; then
    echo
    echo "Creating bin directory in your home directory..."
    mkdir "$HOME/bin"
fi

# Install pacaur.
if [ -x "$( command -v pacaur )" ]; then
    echo
    echo "pacaur is already installed. Moving on..."
else
    echo
    echo "pacaur is not installed yet. Installing now..."

    git clone https://aur.archlinux.org/pacaur-git.git
    cd pacaur-git
    makepkg -sri
    cd ..
    rm -rf pacaur-git
fi

sudo pacman -S --noconfirm python2 python2-pip python python-pip


cd "$HOME/.dotfiles"
# Start the actual installation.
python3 install.py

echo "Installing Python packages ..."
sudo pip install -r packages_python

echo "Done installing your dotfiles."
screenfetch
