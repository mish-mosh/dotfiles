#!/usr/bin/env bash
#
# Configure git related stuff.

echo
echo "#################################"
echo "#"
echo "# Configuration: git"
echo "#"
echo "#################################"

echo
echo "If you don't want to change one of the settings, don't enter anything and press return."

echo -n "Enter your git name: "
read -e git_name
if [[ "$git_name" == "" ]]; then
    git_name=$(git config user.name)
fi

echo -n "Enter your git email: "
read -e git_email
if [[ "$git_email" == "" ]]; then
    git_email=$(git config user.email)
fi

echo "Setting up your git config..."
ln -s $DOTFILES/git/.gitconfig $HOME/.gitconfig

echo "[user]" > $DOTFILES/git/.gitconfig.local
echo "    name = ${git_name}" >> $DOTFILES/git/.gitconfig.local
echo "    email = ${git_email}" >> $DOTFILES/git/.gitconfig.local
ln -s $DOTFILES/git/.gitconfig.local $HOME/.gitconfig.local
